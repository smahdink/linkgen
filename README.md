# linkgen

This little tool asks for a link with * in the location that the user wants the numbering to be done and then asks for range and generates the links in a plain text file to be imported to user's prefered download manager.

Terminal output example:

```
user@computer $: ./linkgen
In God we trust
LINKGEN
Please replace the number you wanna change in the link with a *
LINK (max 300 charachters): example.com/image*.png
Range of *
Min: 1
Max: 25
user@computer $:
```

Output file example:

```
example.com/image01.png
example.com/image02.png
example.com/image03.png
example.com/image04.png
example.com/image05.png
example.com/image06.png
example.com/image07.png
example.com/image08.png
example.com/image09.png
example.com/image10.png
example.com/image11.png
example.com/image12.png
example.com/image13.png
example.com/image14.png
example.com/image15.png
example.com/image16.png
example.com/image17.png
example.com/image18.png
example.com/image19.png
example.com/image20.png
example.com/image21.png
example.com/image22.png
example.com/image23.png
example.com/image24.png
example.com/image25.png
```
